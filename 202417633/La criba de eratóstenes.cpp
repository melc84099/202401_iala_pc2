#include <iostream>
#include "upc.h"

using namespace std;

int* criba_de_eratostenes(int N) {
    int* criba = new int[N + 1];

    
    for (int i = 0; i <= N; ++i) {
        *(criba + i) = 0; 
    }

    
    for (int i = 2; i * i <= N; ++i) {
        if (*(criba + i) == 0) { 
            for (int j = i * i; j <= N; j += i) {
                *(criba + j) = -1; 
            }
        }
    }

    return criba;
}

int main() {
    int N;
    cout << "Ingrese el valor de N: ";
    cin >> N;

    int* criba = criba_de_eratostenes(N);

    cout << "N�meros primos hasta " << N << ":" << endl;
    for (int i = 2; i <= N; ++i) {
        if (*(criba + i) == 0) {
            cout << i << " ";
        }
    }
    cout << endl;

    delete[] criba;

    system("pause");
    return 0;
}

